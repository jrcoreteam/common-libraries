package org.jumin.common.utils.model;


public enum BaseDataType {
	DATE, DATETIME, DOUBLE, FLOAT, INTEGER, ISODATE, ISODATETIME, LONG, STRING;
}
