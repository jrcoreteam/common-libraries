This repository stores common utilities for Java development.

The contents are:
* Authorization Library
* Base Service Library
* EHCache Utilities/Helpers
* Hazelcast Utilities/Helpers
* Java Language Utilities/Helpers
* Preference Library
* Xml Schema Based Processing Library
* Xml Processing Utilities/Helpers

