/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.common.ehcache;

import java.net.URL;
import java.util.List;
import java.util.concurrent.ConcurrentSkipListSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.config.Configuration;
import net.sf.ehcache.config.ConfigurationFactory;

public class CacheManagerFactory {
	
	private static Logger log = LoggerFactory.getLogger(CacheManagerFactory.class);
	
	public static final String DEFAULT_CACHE_MANAGER = "jrDefaultCacheManager";
	public static final String DEFAULT_CACHE = "jrDefaultCache";
	
	private static final ConcurrentSkipListSet<String> LOADED_URL_SET = new ConcurrentSkipListSet<String>(); 
	
	static {
		System.setProperty("net.sf.ehcache.skipUpdateCheck", "true");
	}
	
	public static void shutdownFmpDefaultCacheManager() {
		CacheManager defaultCacheManager = CacheManager.getCacheManager(DEFAULT_CACHE_MANAGER);
		
		if (defaultCacheManager != null) {
			log.info("[CACHE] Shutting down cache manager: " + defaultCacheManager.getName());
			defaultCacheManager.shutdown();
		}
	}
	
	public static void shutdownAllCacheManager() {
		List<CacheManager> cacheManagerList = CacheManager.ALL_CACHE_MANAGERS;
		
		if (cacheManagerList != null && cacheManagerList.size() > 0) {
			for (CacheManager cacheManager : cacheManagerList) {
				log.info("[CACHE] Shutting down cache manager: " + cacheManager.getName());
				cacheManager.shutdown();
            }
		}
	}
	
	public static CacheManager getFmpDefaultCacheManager() {
		return getDefaultCacheManager();
	}
	
	public static CacheManager getDefaultCacheManager() {
		CacheManager defaultCacheManager = CacheManager.getCacheManager(DEFAULT_CACHE_MANAGER);
		
		if (defaultCacheManager == null) {
			defaultCacheManager = CacheManager.newInstance();
			defaultCacheManager.setName(DEFAULT_CACHE_MANAGER);
		}
		
		return defaultCacheManager;
	}
	
	public static Cache createSimpleCache(String cacheName) {
		CacheConfiguration cacheConfig = getDefaultCacheConfiguration();
		cacheConfig.setName(cacheName);
		return createCache(cacheConfig);
	}

	public static Cache createCache(CacheConfiguration cacheConfig) {
		Cache cache = new Cache(cacheConfig);
		return cache;
	}

	public static CacheConfiguration getDefaultCacheConfiguration() {
		URL url = CacheManagerFactory.class.getResource("/ehcache.xml");
		Configuration defaultConfig = ConfigurationFactory.parseConfiguration(url);
		return defaultConfig.getDefaultCacheConfiguration();
	}
	
	public CacheManager addToDefaultCacheManager(URL url) {
		CacheManager cm = getFmpDefaultCacheManager();
		
		if (LOADED_URL_SET.contains(url.toString())) return cm;
		
		Configuration config = ConfigurationFactory.parseConfiguration(url);
		for (String cacheKey : config.getCacheConfigurationsKeySet()) {
			Cache cache = cm.getCache(cacheKey);
			if (cache == null) {
				CacheConfiguration cacheConfig = config.getCacheConfigurations().get(cacheKey);
				cache = new Cache(cacheConfig);
				cm.addCache(cache);
			}
		}
		LOADED_URL_SET.add(url.toString());
		
		return cm;
	}
	
	public CacheManager getCacheManager(String name, URL url) {
		CacheManager cm = CacheManager.getCacheManager(name);
		if (cm == null) {
			Configuration config = ConfigurationFactory.parseConfiguration(url);
			config.setName(name);

			cm = CacheManager.newInstance(config);
		}

		return cm;
	}

}
