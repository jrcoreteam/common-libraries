/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.common.ehcache;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;
import net.sf.ehcache.search.Query;
import net.sf.ehcache.search.expression.Criteria;

public class CacheOperationHelper {

	/**
	 * Store a value to cache with the given key as index.
	 * <br>
	 * @param cache
	 *            cache to operate
	 * @param key
	 * @param value
	 */
	public static void storeToCache(Cache cache, Object key, Object value) {
		cache.put(new Element(key, value));
	}

	/**
	 * Store a value to cache with the given key as index.
	 * <br>
	 * @param cache
	 *            cache to operate
	 * @param key
	 * @param value
	 * @param timeToIdleSeconds
	 * @param timeToLiveSeconds
	 */
	public static void storeToCache(Cache cache, Object key, Object value, int timeToIdleSeconds, int timeToLiveSeconds) {
		cache.put(new Element(key, value, false, timeToIdleSeconds, timeToLiveSeconds));
	}

	/**
	 * Update cache value for the given key.
	 * <br>
	 * @param cache
	 *            cache to operate
	 * @param key
	 * @param value
	 * @return new object value
	 */
	public static <T> T updateCache(Cache cache, Object key, T value) {
		Element existingElement = cache.get(key);
		if (existingElement == null) {
			return null;
		}

		cache.put(new Element(key, value));

		return value;
	}
	
	/**
	 * Update cache value for the given key.
	 * <br>
	 * @param cache
	 *            cache to operate
	 * @param key
	 * @param value
	 * @param timeToIdleSeconds
	 * @param timeToLiveSeconds
	 * @return
	 */
	public static <T> T updateCache(Cache cache, Object key, T value, int timeToIdleSeconds, int timeToLiveSeconds) {
		Element existingElement = cache.get(key);
		if (existingElement == null) {
			return null;
		}

		cache.put(new Element(key, value, false, timeToIdleSeconds, timeToLiveSeconds));

		return value;
	}

	/**
	 * Remove a value from cache for the given key.
	 * <br>
	 * @param cache
	 *            cache to operate
	 * @param key
	 */
	public static void removeFromCache(Cache cache, Object key) {
		cache.remove(key);
	}

	/**
	 * Retrieve value from the cache for the given key.
	 * <br>
	 * @param cache
	 *            cache to operate
	 * @param key
	 * @return object value
	 */
	@SuppressWarnings("unchecked")
	public static <T> T retrieveCacheValue(Cache cache, Object key) {
		if (cache.getSize() < 1)
			return null;

		Element element = cache.get(key);
		if (element == null)
			return null;

		return (T) element.getObjectValue();
	}
	
	public static boolean containsKey(Cache cache, Object key) {
		if (cache.getSize() < 1)
			return false;
		
		return cache.get(key) != null;
	}
	
	/**
	 * Formulate a criteria for all entries in the cache.
	 * @return criteria for all keys
	 */
	public static Criteria getQueryAllCriteria() {
		return Query.KEY.ilike("*");
	}
	
    public static <T> List<T> retrieveAllCacheValues(Cache cache) {
		Map<Object, Element> catalog = cache.getAll(cache.getKeys());

		return getValueListFromElementList(catalog.values());
	}
	
	@SuppressWarnings("unchecked")
    private static <T> List<T> getValueListFromElementList(Collection<Element> elementList) {
		List<T> valueList = new ArrayList<T>();
		
		for (Element element : elementList) {
	        valueList.add((T) element.getObjectValue());
        }
		
		return valueList;
	}
}
