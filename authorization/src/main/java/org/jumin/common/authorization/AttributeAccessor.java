package org.jumin.common.authorization;

public interface AttributeAccessor<T> {

    public <V> V getAttributeValue(T object, String attributeName);
    
}
