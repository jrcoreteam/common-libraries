/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.common.xsutils;

import java.net.URL;

import org.jumin.common.ehcache.CacheManagerFactory;
import org.jumin.common.ehcache.CacheOperationHelper;
import org.xml.sax.SAXException;

import com.sun.xml.xsom.XSSchema;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.config.CacheConfiguration;

/**
 * The class <code>CacheableJrxXmlModelUtil</code> is a version of JrxXmlModelUtil with schema caching mechanism to
 * reduce I/O processing of the XSD loading.
 * 
 */
public class CacheableJrxXmlModelUtil extends JrxXmlModelUtil {

	public static final String XSD_CACHE_NAME = "xsdCache";

	public static final String XSD_URL_CACHE_NAME = "xsdUrlCache";

	private Cache schemaCache = null;

	private Cache schemaUrlCache = null;

	public static CacheableJrxXmlModelUtil newInstance() {
		return new CacheableJrxXmlModelUtil();
	}

	protected CacheableJrxXmlModelUtil() {
		super();
		initCache();
	}

	private void initCache() {
		CacheManager cacheManager = CacheManagerFactory.getDefaultCacheManager();
		if (schemaCache == null) {
			schemaCache = cacheManager.getCache(XSD_CACHE_NAME);
			if (schemaCache == null) {

				CacheConfiguration cacheConfig = CacheManagerFactory.getDefaultCacheConfiguration();
				cacheConfig.setName(XSD_CACHE_NAME);
				cacheConfig.setMaxEntriesLocalHeap(200);

				schemaCache = CacheManagerFactory.createCache(cacheConfig);
				cacheManager.addCacheIfAbsent(schemaCache);
			}
		}

		if (schemaUrlCache == null) {
			schemaUrlCache = cacheManager.getCache(XSD_URL_CACHE_NAME);
			if (schemaUrlCache == null) {

				CacheConfiguration cacheConfig = CacheManagerFactory.getDefaultCacheConfiguration();
				cacheConfig.setName(XSD_URL_CACHE_NAME);
				cacheConfig.setMaxEntriesLocalHeap(200);

				schemaUrlCache = CacheManagerFactory.createCache(cacheConfig);
				cacheManager.addCacheIfAbsent(schemaUrlCache);
			}
		}
	}

	@Override
	public String[] addSchema(URL xmlSchemaUrl) throws SAXException {
		String[] namespaceUriArray = CacheOperationHelper.retrieveCacheValue(schemaUrlCache, xmlSchemaUrl.toString());
		if (namespaceUriArray == null) {
			namespaceUriArray = super.addSchema(xmlSchemaUrl);
			CacheOperationHelper.storeToCache(schemaUrlCache, xmlSchemaUrl.toString(), namespaceUriArray);
			for (String namespaceUri : namespaceUriArray) {
				XSSchema xsSchema = super.getSchema(namespaceUri);
				CacheOperationHelper.storeToCache(schemaCache, namespaceUri, xsSchema);
			}
		}

		return namespaceUriArray;
	}

	@Override
	public String addSchema(XSSchema xsSchema) {
		String namespaceUri = xsSchema.getTargetNamespace();
		if (!CacheOperationHelper.containsKey(schemaCache, namespaceUri)) {
			CacheOperationHelper.storeToCache(schemaCache, namespaceUri, xsSchema);
		}

		return super.addSchema(xsSchema);
	}

	@Override
	public XSSchema getSchema(String namespaceUri) {
		XSSchema xsSchema = super.getSchema(namespaceUri);
		if (xsSchema != null)
			return xsSchema;

		xsSchema = CacheOperationHelper.retrieveCacheValue(schemaCache, namespaceUri);
		if (xsSchema != null) {
			super.addSchema(xsSchema);
		}

		return xsSchema;
	}

	@Override
	protected boolean hasSchemas() {
		return schemaCache.getSize() > 0;
	}

	@Override
	protected boolean hasSchema(String namespaceUri) {
		return CacheOperationHelper.containsKey(schemaCache, namespaceUri);
	}

}
